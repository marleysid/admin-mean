var mongoose = require('mongoose');


//create users schema for storing user datas
var userSchema = new mongoose.Schema({
  email: String,
  username: String,
  password: String,
  created_at: {
    type: Date,
    default: Date.now
  }
});


//creating blog schema

var postSchema = new mongoose.Schema({
	title: String,
	description: String,
	/*author: [{
			type: mongoose.Schema.Types.ObjectId,
			ref: 'User',
		}]*/
    author: String
});



//creating author schema
var authorSchema = new mongoose.Schema({
  firstname: String,
  lastname: String,
  address: String,
  dateofbirth: Date
});

//declare models
mongoose.model('User', userSchema);
mongoose.model('Post', postSchema);
mongoose.model('Author', authorSchema);