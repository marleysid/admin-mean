// app.js
var meanApp = angular.module('meanApp', ['ui.router', 'ngMessages', 'ngResource', 'ngCookies']).run(function($rootScope, $location, $http, $cookies) {
    $rootScope.authenticated = false;
    $rootScope.current_user = '';


    //implementing signout functionality
    $rootScope.signout = function() {
        $http.get('auth/signout');
        $rootScope.authenticated = false;
        $rootScope.current_user = '';
        $cookies.remove("user");
    }

    //check whether the cookie exists or not
     $rootScope.$on("$locationChangeStart", function(event, next, current) { 
            //check here whether the cookie exists or not. if it doesnot exists, then throw user back to login
            if ($cookies.get('user') != null){
                var user = JSON.parse($cookies.get('user'));
                $rootScope.authenticated = true;
                $rootScope.current_user = user;  
            } else {
                $location.path('/');
            }
    });
});



meanApp.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
    $stateProvider.state('common', {
        abstract: true,
        views: {
            '': {
                templateUrl: 'shared/common.html'
            },
            'header@common': {
                templateUrl: 'shared/header.html'
            },
            'sidebar@common': {
                templateUrl: 'shared/sidebar.html'
            },
            'script@common': {
                templateUrl: 'shared/script-init.html'
            },
        },
    }).state('login', {
        url: '/',
        templateUrl: 'login.html',
        controller: 'authController'
    }).state('dashboard', {
        url: '/dashboard',
        parent: 'common',
        views: {
            '': {
                templateUrl: 'components/main.html',
                controller: ''
            },
        }
    })
});

meanApp.controller('authController', function($scope, $http, $location, $rootScope, $cookies) {
    $scope.user = {
        email: '',
        password: ''
    };
    $scope.error_message = '';
    $scope.login = function() {
        $http.post('/auth/login', $scope.user).success(function(data) {
            if (data.state == "success") {
                //add user to the cookies
                $cookies.put('user', JSON.stringify(data.user));
                //get the essential datas of currently logged in user
                $rootScope.authenticated = true;
                $rootScope.current_user = data.user;
                $location.path('/dashboard');
            } else {
                $scope.error_message = data.message;
            }
        });
    }
});