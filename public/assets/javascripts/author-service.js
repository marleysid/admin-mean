    // super simple service
    // each function returns a promise object 
    meanApp.factory('authorService', function($http) {
        return {
            get : function() {
                return $http.get('/api/author');
            },
            create : function(author) {
                return $http.post('/api/author', author);
            },

            edit : function(id){
                return $http.get('/api/author/' + id);
            },

            update: function(author, id){
                return $http.put('/api/author/' + id, author);
            },

            delete : function(id) {
                return $http.delete('/api/author/' + id);
            }
        }
    });



