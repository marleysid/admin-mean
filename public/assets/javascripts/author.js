meanApp.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider.state('author', {
        url: '/author/list',
        parent: 'common',
        views: {
            '': {
                templateUrl: 'components/author/list.html',
                controller: 'authorController'
            },
        }
    }).state('author.add', {
        url: '/author/add',
        parent: 'common',
        views: {
            '': {
                templateUrl: 'components/author/add.html',
                controller: 'authorController'
            }
        }
    }).state('author.edit',{
        url: '/author/edit/{id}',
        parent: 'common',
        views: {
            '': {
                templateUrl: 'components/author/edit.html',
                controller: 'authorEditController'
            }
        }
    })


    .state('author.delete', {
        url: '/author/delete/{id}',
        parent: 'common',
        views: {
            '': {
                controller: 'authorController'
            }
        }
    })
});
meanApp.controller('authorController', function($scope, $location, $http, authorService, $stateParams, $state) {
    //listing the authors
    authorService.get().success(function(data) {
        $scope.author = data;

    });


    $scope.addAuthor = function(isValid) {
            if (isValid) {
                $scope.newAuthor = {
                    firstname: '',
                    lastname: '',
                    address: '',
                    dateofbirth: ''
                };
                $scope.newAuthor.firstname = $scope.author.firstname;
                $scope.newAuthor.lastname = $scope.author.lastname;
                $scope.newAuthor.address = $scope.author.address;
                $scope.newAuthor.dateofbirth = $scope.author.dateofbirth;
                // call the create function from our service (returns a promise object)
                authorService.create($scope.newAuthor)
                    // if successful creation, call our get function to get all the new todos
                    .success(function(data) {
                        $scope.newAuthor = {}; // clear the form so our user is ready to enter another
                        // $scope.author = data; // assign our new list of todos
                    })
                     .error(function(data) {
                        $scope.error_message = "Date error";
                    });
            } else {
                //submitted form is invalid
                $scope.error_message = "You have not entered anything or you are missing something. Please check";
            }
        }

        //deleting author
    $scope.deleteAuthor = function(id) {
        authorService.delete(id)
            // if successful creation, call our get function to get all the new author
            .success(function(data) {
                $scope.author = data; // assign our new list of todos
                $location.path('/author/list');
            });
    }
});


//editing and updating the author
meanApp.controller('authorEditController', function($scope, authorService, $location, $stateParams){
    $scope.id = $stateParams.id;
    $scope.author = authorService.edit($stateParams.id)
                    .success(function(data){
                    console.log(data);                   
                    $scope.author = data;
                });

    //updating the author detail
    $scope.updateAuthor = function(){
      authorService.update($scope.author,$scope.author._id).success(function(){
            $location.path('/author/list');
      });
    }
});

