meanApp.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider.state('blog', {
        url: '/blog/add',
        parent: 'common',
        views: {
            '': {
                templateUrl: 'components/blog/add.html'
            },
            'breadcrumb@blog': {
                templateUrl: 'shared/breadcrumb.html'
            },
        },
        controller: 'postController'
    }).state('blog.list', {
        url: '/blog/list',
        parent: 'common',
        views: {
            '': {
                templateUrl: 'components/blog/list.html',
                controller: 'postController',
            },
        },
    }).state('blog.edit', {
        url: '/blog/edit/{id}',
        parent: 'common',
        views: {
            '': {
                templateUrl: 'components/blog/edit.html',
                controller: 'postEditController',
            },
        },
    }).state('blog.delete', {
        url: '/blog/delete/{id}',
        parent: 'common',
        views: {
            '': {
                controller: 'postDeleteController',
            },
        },
    })
});
meanApp.factory('postService', function($resource) {
    return $resource('/api/posts/:id');
});
//add new post
meanApp.controller('postController', function(postService, $scope, $http, $location) {
    $scope.blog = postService.query();
    $scope.newBlog = {
        title: '',
        description: '',
        author: ''
    };
    $scope.addpost = function(isValid) {
        if (isValid) {
            $scope.newBlog.title = $scope.blog.title;
            $scope.newBlog.description = $scope.blog.description;
            $scope.newBlog.author = $scope.blog.author;
            postService.save($scope.newBlog, function() {
                $scope.blog = postService.query();
                $scope.newBlog = {
                    title: '',
                    description: '',
                    author: ''
                };
                $location.path('/blog/list');
            });
        } else {
            $scope.error_message = "Invalid form inputs. Please check";
        }
    };
});
//edit the post
meanApp.controller('postEditController', function(postService, $stateParams, $scope, $http, $location) {
    $scope.blog = $http.get("/api/posts/" + $stateParams.id).success(function(response) {
        $scope.blog = response;
        console.log(response);
    });
    //update the post
    $scope.editPost = function() {
        console.log($scope.blog._id);
        $http.put('/api/posts/' + $scope.blog._id, $scope.blog).success(function(response) {
            console.log("updated successfully");
            $location.path('/blog/list');
            //refresh();
        });
    }
});
//delete post
meanApp.controller('postDeleteController', function($stateParams, $scope, $http, $location, $state) {
    $http.delete('/api/posts/' + $stateParams.id).success(function(response) {
        $location.path('/blog/list');
        //refresh();
    });
});