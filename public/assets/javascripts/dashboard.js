

myApp.config(function($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('dashboard', {
      url: '/dashboard',
      views: {
        '': {
          templateUrl: 'pages/admin/dashboard.html',
          controller: 'dashboardController',
        },
      }
    });

});


myApp.controller('dashboardController', function(){

});


