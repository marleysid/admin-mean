var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Author = mongoose.model('Author');


//Used for routes that must be authenticated.
function isAuthenticated(req, res, next) {
	// if user is authenticated in the session, call the next() to call the next request handler 
	// Passport adds this method to request object. A middleware is allowed to add properties to
	// request and response objects

	if (req.isAuthenticated()) {
		return next();
	}

	// if the user is not authenticated then redirect him to the login page
	return res.redirect('/#login');
};

//Register the authentication middleware
router.use('/author', isAuthenticated);

//api for all the Author CRUD operation
router.route('/author')

//getting all the author list
.get(function(req, res){
	Author.find(function(err, data){
		if (err){
			return res.send(error, 500);
		}
		return res.json(data);
	});
})

// /api/author POST method
.post(function(req, res){
	//create a new instance of the author
	var author = new Author();
	author.firstname   = req.body.firstname;
	author.lastname    = req.body.lastname;
	author.address     = req.body.address;
	author.dateofbirth = req.body.dateofbirth;

	author.save(function(err, author){
		if (err){
			return res.send(500, err);
		}
		return res.json(author);
	});
});

router.route('/author/:id')
//get author by Id
.get(function(req, res){
	Author.findById(req.params.id, function(err, author){
		if (err){
			return res.send(500, err);
		} 
		return res.json(author);
	});
})

//update author by Id
.put(function(req, res){
	Author.findById(req.params.id, function(err, author){
		if (err){
			return res.send(500, err);
		}
		author.firstname   = req.body.firstname;
		author.lastname    = req.body.lastname;
		author.address     = req.body.address;
		author.dateofbirth = req.body.dateofbirth;

		author.save(function(err, author){
				if (err){
					return res.send(500, err);
				}	
				return res.json(author);
		});

	});
})


//delete author by Id
.delete(function(req, res){
	Author.remove({_id: req.params.id}, function(err){
		if (err){
			return res.send(500, err);
		}
		return res.json({message: "User with id " + req.params.id + " removed successfully"});
	})
});

module.exports = router;